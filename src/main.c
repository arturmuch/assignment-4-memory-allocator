#include <assert.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

#define MAP_ANONYMOUS 0x20
#define FIRST_BLOCK_SIZE 32
#define SECOND_BLOCK_SIZE 64
#define THIRD_BLOCK_SIZE 512
#define BLOCK_SIZE_12345 12345
#define FIRST_BLOCK_SIZE_LARGE 16384
#define REG_MIN_SIZE 4096

static struct block_header *get_block_header(void *addr) {
    return (struct block_header *)(addr - offsetof(struct block_header, contents));
}

static void test_allocation() {
    printf("\n###test_allocation###\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *mem = _malloc(0);
    assert(mem != NULL && "###allocation_failed###");

    debug_heap(stdout, heap);
    heap_term();
    printf("###allocations_success###\n");
}

static void test_malloc_and_free() {
    printf("\n###test_malloc_and_free###\n");
    void *heap = heap_init(0);

    void *first_block = _malloc(FIRST_BLOCK_SIZE);
    struct block_header *first_block_header = get_block_header(first_block);
    assert(first_block != NULL && first_block_header->capacity.bytes == FIRST_BLOCK_SIZE && "###first_block_malloc_fail###");
    void *second_block = _malloc(SECOND_BLOCK_SIZE);
    struct block_header *second_block_header = get_block_header(second_block);
    assert(second_block != NULL && second_block_header->capacity.bytes == SECOND_BLOCK_SIZE && "###second_block_malloc_fail###");
    void *third_block = _malloc(THIRD_BLOCK_SIZE);
    struct block_header *third_block_header = get_block_header(third_block);
    assert(third_block != NULL && third_block_header->capacity.bytes == THIRD_BLOCK_SIZE && "###third_block_malloca_fail###");
    debug_heap(stdout, heap);

    _free(second_block);
    assert(second_block_header->is_free && "###second_block_free_fail###");
    debug_heap(stdout, heap);

    _free(third_block);
    assert(third_block_header->is_free && "###third_block_free_fail###");
    _free(first_block);
    assert(first_block_header->is_free && "###first_block_free_fail###");
    debug_heap(stdout, heap);

    heap_term();
    printf("###free_and_malloc_success###\n");
}

static void test_region_extension() {
    printf("\n###reg_extension_test###\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *block = _malloc(BLOCK_SIZE_12345);
    assert(block != NULL && "###reg_extension_failed###");
    _free(block);
    debug_heap(stdout, heap);

    heap_term();
    printf("###reg_extension_success###\n");
}

static void test_region_extension_another_location() {
    printf("\n###reg_extension_another_loc_test###\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *blocker = mmap(heap + REG_MIN_SIZE, REG_MIN_SIZE, PROT_READ | PROT_WRITE,
                         MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
    assert(blocker != MAP_FAILED);
    void *first_block = _malloc(FIRST_BLOCK_SIZE_LARGE);
    void *second_block = _malloc(0);
    assert(get_block_header(second_block) != heap + REG_MIN_SIZE && "###reg_extension_another_loc_failed###");

    debug_heap(stdout, heap);
    _free(first_block);
    _free(second_block);
    munmap(blocker, REG_MIN_SIZE);
    heap_term();
    printf("###reg_extension_another_loc_success###\n");
}

int main() {
    test_allocation();
    test_malloc_and_free();
    test_region_extension();
    test_region_extension_another_location();
    return 0;
}
